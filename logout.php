<?php
session_start();
$_SESSION['loginProfile'] = NULL; //loginProfile不登錄

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>logged out!</title>
</head>
<style type="text/css">
    body{
        font-family: fantasy;
        background-image: linear-gradient(to right,#f7f6e8 ,#f7f6e8,#f5f4e1,#f5f4e1);
		
    }
    
    #pizza {
            /* 固定在一個位置 */
            position: fixed;
            right: 0;
            bottom: 0;
            /* 把它放在圖片底下 */
            z-index: -1;
        }
    #watermelon {
        width: 470px;
        position: fixed;
        left:100px;
        bottom: 0;
        /* 把它放在圖片底下 */
        z-index: -1;
    }
</style>
<body  style="text-align:center;">
<img id="pizza" src="https://media.giphy.com/media/l3q2Dh5BA4zRFSwak/giphy.gif" >
<img id="watermelon" src="https://media.giphy.com/media/3oz8xDzuVDbKoU4shi/giphy.gif">
<p>You are logged out!</p>
<hr>
<a href='loginUI.php'>Login again</a>

</body>
</html>

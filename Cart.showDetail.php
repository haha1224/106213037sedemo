<?php
session_start();
require("orderModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Detail</title>
</head>
<style type="text/css">
    body{
        font-family: fantasy;
        background-image: linear-gradient(to right,#f7f6e8 ,#f7f6e8,#f5f4e1,#f5f4e1);
		
    }
    
    #pizza {
            /* 固定在一個位置 */
            position: fixed;
            right: 0;
            bottom: 0;
            /* 把它放在圖片底下 */
            z-index: -1;
        }
    #watermelon {
        width: 470px;
        position: fixed;
        left:100px;
        bottom: 0;
        /* 把它放在圖片底下 */
        z-index: -1;
    }
</style>
<body>
<img id="pizza" src="https://media.giphy.com/media/l3q2Dh5BA4zRFSwak/giphy.gif" >
<img id="watermelon" src="https://media.giphy.com/media/3oz8xDzuVDbKoU4shi/giphy.gif">
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"]," [",'<a href="logout.php">logout</a>',"]";
	$result=getCartDetail($_SESSION["loginProfile"]["uID"]); //取得購物車資訊
?>
<hr>
	<table width="200" border="1">
  <tr>
    <td>id</td>
    <td>Prd Name</td>
    <td>price</td>
    <td>Quantity</td>
    <td>Amount</td>
	<td>-</td>
  </tr>
<?php
$total=0;
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['serno'] . "</td>";
	echo "<td>{$rs['name']}</td>";
	echo "<td>" , $rs['price'], "</td>";
	echo "<td>" , $rs['quantity'], "</td>";
	$total += $rs['quantity'] *$rs['price'];
	echo "<td>" , $rs['quantity'] *$rs['price'] , "</td>";
	echo "<td><a href='Cart.removeItem.php?serno=" , $rs['serno'] , "'>Remove</a></td></tr>";
}
echo "<tr><td>Total: $total</td></tr>";
?>
</table>
<hr>
<a href="order.confirm.php">Yes Checkout</a>  <a href="main.php">No, keep shopping</a>
</body>
</html>

<?php
require_once("dbconfig.php");

function getOrderList($uID) {
	global $db;
	$sql = "SELECT ordID, orderDate, status FROM userorder WHERE uID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getConfirmedOrderList() {//回傳userOrder的ordID、uID
	global $db;
	$sql = "SELECT ordID, uID, orderDate FROM userorder WHERE status=1";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	//mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function _getCartID($uID) {//取得購物車編號
	//get an unfished order (status=0) from userOrder
	global $db;
	$sql = "SELECT ordID FROM userorder WHERE `uID`= ? and `status`=0";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	if ($row=mysqli_fetch_assoc($result)) {
		return $row["ordID"];
	} else {
		//no order with status=0 is found, which means we need to creat an empty order as the new shopping cart
		$sql = "INSERT INTO userorder (`uID`, `orderDate`, `address`, `status`) VALUES (?, '2019-01-01', '', '0')";
		$stmt = mysqli_prepare($db, $sql); //prepare sql statement
		mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
		mysqli_stmt_execute($stmt);  //執行SQL
		$newOrderID=mysqli_insert_id($db);
		return $newOrderID;
	}
}

function addToCart($uID, $prdID) { //加入購物車
	global $db;
	$ordID= _getCartID($uID);
	$sql = "insert into orderitem (ordID, prdID, quantity) values (?,?,1);";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function updateCart($uID, $prdID) {//用來更新商品數量
	global $db;
	$ordID = _getCartID($uID);
	$sql = "update orderitem set quantity = quantity + 1 where prdID=? and ordID=$ordID";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $prdID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
}

function getprdID($uID, $prdID) {//回傳整個 orderitem 的 prdID
	global $db;
	$ordID= _getCartID($uID);
	$sql = "SELECT prdID FROM orderitem WHERE ordID = $ordID";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	//mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}
function removeFromCart($serno) { //刪除產品
	global $db;
	$sql = "delete from orderItem where serno=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $serno); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}
// function checkout($uID, $address) { //完成下訂單 把status 變成 1 和 自動輸入下單日期
// 	global $db;
// 	$ordID= _getCartID($uID);
// 	$sql = "insert into userorder (orderDate,adderss,status,uID,) values (now(),?,1,?)";
// 	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
// 	mysqli_stmt_bind_param($stmt, "ss", $address, $uID); //bind parameters with variables
// 	return mysqli_stmt_execute($stmt);  //執行SQL
//  }

//完成下訂單 把status 變成 1 和 自動輸入下單日期
function checkout($uID, $address) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "update userorder set orderDate=123,address=?,status=1 where ordID=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "si", $address, $ordID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}
function shipout($ordID) { //更改訂單狀態
	global $db;
	$sql = "update userorder set status=2 where ordID=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function getCartDetail($uID) { //取得購物車資訊
	global $db;
	$ordID= _getCartID($uID);
	$sql="select orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}


function getOrderDetail($ordID) { //取得訂單資訊
	global $db;
	$sql="select orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}
?>











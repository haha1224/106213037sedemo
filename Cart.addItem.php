<?php
session_start();
//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}

require("orderModel.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Item Added</title>
</head>
<style type="text/css">
    body{
        font-family: fantasy;
        background-image: linear-gradient(to right,#f7f6e8 ,#f7f6e8,#f5f4e1,#f5f4e1);
		
    }
    
    #pizza {
            /* 固定在一個位置 */
            position: fixed;
            right: 0;
            bottom: 0;
            /* 把它放在圖片底下 */
            z-index: -1;
        }
    #watermelon {
        width: 470px;
        position: fixed;
        left:100px;
        bottom: 0;
        /* 把它放在圖片底下 */
        z-index: -1;
    }
</style>
<body style="text-align:center;">
<img id="pizza" src="https://media.giphy.com/media/l3q2Dh5BA4zRFSwak/giphy.gif" >
<img id="watermelon" src="https://media.giphy.com/media/3oz8xDzuVDbKoU4shi/giphy.gif">
<?php
//取得哪個使用者
$uID=$_SESSION['loginProfile']['uID'];
//取得添加商品的ID
$prdID=(int)$_GET['prdID'];
$result = getprdID($uID, $prdID);//整個 orderitem 的 prdID
//$up 用來判斷是否有資料
$up = 1;
while($rs=mysqli_fetch_assoc($result)){
	
	if($rs['prdID']== $prdID){
		updateCart($uID, $prdID);//更新他
		$up = 0;
		break;
	}
}
if($up == 1){//如果不曾加入購物車，則直接加入
	addToCart($uID,$prdID);
}
?>
Item Added!! <br>
<a href="main.php">Back to main Page</a>

</body>
</html>

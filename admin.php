<?php
session_start();
require("orderModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
	if ($_SESSION['loginProfile']['uRole'] < 9) {
		header("Location: main.php");
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin</title>
</head>
<style type="text/css">
    body{
        font-family: fantasy;
        background-image: linear-gradient(to right,#f7f6e8 ,#f7f6e8,#f5f4e1,#f5f4e1);
		
    }
    
    #pizza {
            /* 固定在一個位置 */
            position: fixed;
            right: 0;
            bottom: 0;
            /* 把它放在圖片底下 */
            z-index: -1;
        }
    #watermelon {
        width: 470px;
        position: fixed;
        left:100px;
        bottom: 0;
        /* 把它放在圖片底下 */
        z-index: -1;
    }
</style>
<body>
<img id="pizza" src="https://media.giphy.com/media/l3q2Dh5BA4zRFSwak/giphy.gif" >
<img id="watermelon" src="https://media.giphy.com/media/3oz8xDzuVDbKoU4shi/giphy.gif">

<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"]," [",'<a href="logout.php">logout</a>',"]"
	," [",'<a href="prdMain.php">Product Management</a>',"]";
	$result=getConfirmedOrderList();//回傳userOrder的ordID、uID
?>
	<table width="200" border="1">
  <tr>
    <td>id</td>
    <td>name</td>
    <td>date</td>
    <td>+</td>
  </tr>
<?php
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['ordID'] . "</td>";
	echo "<td>{$rs['uID']}</td>";
	echo "<td>" , $rs['orderDate'], "</td>";
	echo "<td><a href='order.showDetail.php?ID=" , $rs['ordID'] , "'>ShowDetail</a></td></tr>";
}
?>
</table>
</body>
</html>

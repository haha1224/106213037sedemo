<?php
session_start();
//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
require("prdModel.php");
$prdID=(int)$_POST['pID'];

if ($prdID==0) {
	echo "Invalid Parameters!!";

} else {
	$prdDetail=array('prdID' => $prdID, 'name' => $_POST['name'],
		'price' => (int)$_POST['price'],'detail' => $_POST['detail']);

	if ($prdID>0 ) {
		updateProduct($prdID, $prdDetail);
	} else {
		addProduct($prdDetail);	
	}

	echo "Data Saved<br>";
}
?>
<style type="text/css">
    body{
        font-family: fantasy;
        background-image: linear-gradient(to right,#f7f6e8 ,#f7f6e8,#f5f4e1,#f5f4e1);
		
    }
    
    #pizza {
            /* 固定在一個位置 */
            position: fixed;
            right: 0;
            bottom: 0;
            /* 把它放在圖片底下 */
            z-index: -1;
        }
    #watermelon {
        width: 470px;
        position: fixed;
        left:100px;
        bottom: 0;
        /* 把它放在圖片底下 */
        z-index: -1;
    }
</style>

<body style="text-align:center">
<img id="pizza" src="https://media.giphy.com/media/l3q2Dh5BA4zRFSwak/giphy.gif" >
<img id="watermelon" src="https://media.giphy.com/media/3oz8xDzuVDbKoU4shi/giphy.gif">
<br>
<a href="prdMain.php">Back</a>
</body>

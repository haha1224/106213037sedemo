<?php
session_start();
require("prdModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Exit</title>
</head>
<style type="text/css">
    body{
        font-family: fantasy;
        background-image: linear-gradient(to right,#f7f6e8 ,#f7f6e8,#f5f4e1,#f5f4e1);
		
    }
    
    #pizza {
            /* 固定在一個位置 */
            position: fixed;
            right: 0;
            bottom: 0;
            /* 把它放在圖片底下 */
            z-index: -1;
        }
    #watermelon {
        width: 470px;
        position: fixed;
        left:100px;
        bottom: 0;
        /* 把它放在圖片底下 */
        z-index: -1;
    }
</style>
<body>
<img id="pizza" src="https://media.giphy.com/media/l3q2Dh5BA4zRFSwak/giphy.gif" >
<img id="watermelon" src="https://media.giphy.com/media/3oz8xDzuVDbKoU4shi/giphy.gif">

<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"]," [",'<a href="logout.php">logout</a>',"]";

	$prdID=(int)$_GET['id'];
	$rs=NULL;
	if($result=getPrdDetail($prdID)) {//取得產品資料
		$rs=mysqli_fetch_assoc($result);
	}
	if (! $rs) {
		$rs['prdID']=-1;
		$rs['name']='';
		$rs['price']=0;
		$rs['detail']='';
	}

?>
<form action="prd.update.php" method="POST">
	<table width="200" border="1">
  <tr>
    <td>id: <input type="hidden" name="pID" value="<?php echo htmlspecialchars($rs['prdID']);?>"></td></tr>
    <tr><td>name:<input type="text" name="name" value="<?php echo htmlspecialchars($rs['name']);?>"></td></tr>
    <tr><td>price:<input type="text" name="price" value="<?php echo htmlspecialchars($rs['price']);?>"></td></tr>
    <tr><td>detail:<input type="text" name="detail" value="<?php echo htmlspecialchars($rs['detail']);?>"></td></tr>
<tr><td><input type="submit"><td></tr>
</form>

</table>
<a href="prdMain.php">back</a><hr>

</body>
</html>
